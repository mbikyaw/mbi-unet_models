import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="unet_models",
    version="0.0.2",
    author="Xianbin Yong",
    author_email="e0357915@u.nus.edu",
    description="Unet models for the MBI community",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/xianbin.yong13/mbi-unet_models",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)