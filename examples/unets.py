import os

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import matplotlib.pyplot as plt

# import user classes
from unet_models.Unet import Unet
from unet_models.Unet_Resnet import Unet_Resnet101, Unet_Resnet50, Unet_Resnet_paper


# functions for visualization
def display_images(image, cmap='gray', norm=None, interpolation='bilinear'):

    plt.figure(figsize=(14, 14))
    plt.axis('off')
    plt.imshow(image, cmap=cmap,
               norm=norm, interpolation=interpolation)
    plt.show()

model = Unet_Resnet50(model_dir = './Networks/', config_filepath="./default_singleclass_unet.yml")


model.load_dataset("./data/OneClass/Train")
model.augment_images()

model.initialize_model()

import time
start = time.time()
model.train_model()
end = time.time()
print("\nTraining time taken: %1.0f s \n" % (end - start))


